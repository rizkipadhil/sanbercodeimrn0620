/**
 * Berikut soal quiz kali ini, terdiri dari 3 Soal
 * Kerjakan dengan sebaik mungkin, dengan menggunakan metode yang telah dipelajari,
 * Tidak diperkenankan untuk menjawab hanya dengan console.log('teks jawaban');
 * maupun dengan terlebih dahulu memasukkannya ke dalam variabel, misal var a = 'teks jawaban'; console.log(a);
 * 
 * Terdapat tambahan poin pada setiap soal yang dikerjakan menggunakan sintaks ES6 (+5 poin)
 * Jika total nilai Anda melebihi 100 (nilai pilihan ganda + coding), tetap akan memiliki nilai akhir yaitu 100
 * 
 * Selamat mengerjakan
*/

/*========================================== 
  1. SOAL CLASS SCORE (10 poin + 5 Poin ES6)
  ==========================================
  Buatlah sebuah class dengan nama Score. class Score tersebut memiliki properti "subject", "points", dan "email". 
  "points" dapat di input berupa number (1 nilai) atau array of number (banyak nilai).
  tambahkan method average untuk menghitung rata-rata dari parameter points ketika yang di input berupa array (lebih dari 1 nilai)
*/
console.log('Soal 1');
const Score = class {
  // Code disini
  constructor(subject, points, email) {
    this.subject = subject;
    this._points = points;
    this.email = email;
  }
  get points() {
    return this._points;
  }
  set points(x) {
    this._points = x;
  }
  average(){
    let point = this._points;
    let score = 0;
    if (Array.isArray(point)) {
      point.forEach(element => {
        score += element;
      });
      score = score/point.length;
      return score;
    }
    score += this._points/1;
    return score;
  }
  averageSiswa(index){
    const average = this.average();
    let graduate = '';
    if (average > 90) {
      graduate = 'honour';
    }else if(average > 80){
      graduate = 'graduate';
    } else {
      graduate = 'participant';
    }
    let text = `${index}. Email: ${this.email} \nRata- rata: ${average} \nPredikat: ${graduate}`;
    return text;
  }
}

const soal1  = new Score('quiz', 2, 'rizkipadhil@gmail.com');
console.log(soal1.average());
soal1.points = [
  100,20,40,1,2,99,12
];
console.log(soal1.average());

/*=========================================== 
  2. SOAL Create Score (10 Poin + 5 Poin ES6)
  ===========================================
  Membuat function viewScores yang menerima parameter data berupa array multidimensi dan subject berupa string
  Function viewScores mengolah data email dan nilai skor pada parameter array 
  lalu mengembalikan data array yang berisi object yang dibuat dari class Score.
  Contoh: 

  Input
   
  data : 
  [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88]
  ]
  subject: "quiz-1"

  Output 
  [
    {email: "abduh@mail.com", subject: "quiz-1", points: 78},
    {email: "khairun@mail.com", subject: "quiz-1", points: 95},
  ]
*/
console.log('Soal 2');
const data = [
  ["email", "quiz - 1", "quiz - 2", "quiz - 3"],
  ["abduh@mail.com", 78, 89, 90],
  ["khairun@mail.com", 95, 85, 88],
  ["bondra@mail.com", 70, 75, 78],
  ["regi@mail.com", 91, 89, 93]
]

const viewScores = (data, subject) => {
  // code kamu di sini
  const quiz = {
    'quiz-1': 1,
    'quiz-2': 2,
    'quiz-3': 3
  };
  let result = [];
  data.forEach((element, index) => {
    if (index > 0) {
      let makeObject = {
        email: element[0],
        subject: subject,
        points: element[quiz[subject]]
      };
      result = [
        ...result,
        makeObject
      ];
    }
  });
  console.log(result);
}

// TEST CASE
viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")

/*==========================================
  3. SOAL Recap Score (15 Poin + 5 Poin ES6)
  ==========================================
    Buatlah fungsi recapScore yang menampilkan perolehan nilai semua student. 
    Data yang ditampilkan adalah email user, nilai rata-rata, dan predikat kelulusan. 
    predikat kelulusan ditentukan dari aturan berikut:
    nilai > 70 "participant"
    nilai > 80 "graduate"
    nilai > 90 "honour"

    output:
    1. Email: abduh@mail.com
    Rata-rata: 85.7
    Predikat: graduate

    2. Email: khairun@mail.com
    Rata-rata: 89.3
    Predikat: graduate

    3. Email: bondra@mail.com
    Rata-rata: 74.3
    Predikat: participant

    4. Email: regi@mail.com
    Rata-rata: 91
    Predikat: honour

*/
const makeAverage = (index, data, email) => {
  let soal3 = new Score('quiz', data, email);
  return soal3.averageSiswa(index);
};
const recapScores = data => {
  // code kamu di sini
  let result = '';
  data.forEach((element, index) => {
    if (index > 0) {
      let pointarray = [];
      for (let a = 1; a <= 3; a++) {
        pointarray = [
          ...pointarray,
          element[a]
        ];
      }
      result += `${makeAverage(index, pointarray, element[0])} \n`;
    }
  });
  console.log(result);
}

recapScores(data);
