import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// import YoutubeUI from './Tugas/Tugas12/App';
import Tugas13 from './Tugas/Tugas13/App';
import Tugas14 from './Tugas/Tugas14/App';
import Tugas15 from './Tugas/Tugas15/index';
import Tugas15Soal2 from './Tugas/Tugas15/Soal2/App';
import Quiz3 from './Tugas/Quiz3/index';
import Tugasakhir from './Tugas/Tugasakhir/index';
import { Provider } from "react-redux";
import store from "./store";

const App = () => {
  return (
    <Provider store={store}>
      <Tugasakhir />
    </Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title__app: {
    fontSize: 18,
    color: '#ebab34'
  }
});

export default App;
