import React from 'react';
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, Dimensions, TextInput, Button } from 'react-native';

import data from './data.json';

const DEVICE = Dimensions.get('window');

export default class HomeScreen extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      searchText: '',
      totalPrice: 0,
    }
  }

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  updatePrice(price) {
    //? #Soal Bonus (10 poin) 
    //? Buatlah teks 'Total Harga' yang akan bertambah setiap kali salah satu barang/item di klik/tekan.
    //? Di sini, buat fungsi untuk menambahkan nilai dari state.totalPrice dan ditampilkan pada 'Total Harga'.
    const total = this.state.totalPrice + Number(price);
    this.setState({
      totalPrice: total,
    });
    // Kode di sini
  };


  render() {
    console.log(data)

    const listData = (
      data['produk'].map((val, key) => {
        return <ListItem key={val.id} keyval={key} data={val} actionPress={(price) => this.updatePrice(price)} />
      })
    );
    return (
      <View style={styles.container}>
        <View style={{ minHeight: 50, width: DEVICE.width * 0.88 + 20, marginVertical: 8 }}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <Text>Hai,{'\n'}
            {/* //? #Soal 1 Tambahan, Simpan userName yang dikirim dari halaman Login pada komponen Text di bawah ini */}
            <Text style={styles.headerText}>{this.props.route.params.userName}</Text>
            </Text>

            {/* //? #Soal Bonus, simpan Total Harga dan state.totalPrice di komponen Text di bawah ini */}
            <Text style={{ textAlign: 'right' }}>Total Harga{'\n'}
              <Text style={styles.headerText}>{this.currencyFormat(this.state.totalPrice)}</Text>
            </Text>
          </View>
          <View>

          </View>
          <TextInput
            style={{ backgroundColor: 'white', marginTop: 8 }}
            placeholder='Cari barang..'
            onChangeText={(searchText => this.setState({ searchText }))}
          />
        </View>

        {/* 
        //? #Soal No 2 (15 poin)
        //? Buatlah 1 komponen FlatList dengan input berasal dari data.json
        //? dan pada prop renderItem menggunakan komponen ListItem -- ada di bawah --
        //? dan memiliki 2 kolom, sehingga menampilkan 2 item per baris (horizontal)
        
        // Lanjutkan di bawah ini!
        */}
        <View style={{ flex:1, flexDirection: 'row', flexWrap: 'wrap',alignItems: 'flex-start', justifyContent:'center' }}>
          {listData}
        </View>

      </View>
    )
  }
};

class ListItem extends React.Component {

  currencyFormat(num) {
    return 'Rp ' + num.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.')
  };

  //? #Soal No 3 (15 poin)
  //? Buatlah styling komponen ListItem, agar dapat tampil dengan baik di device

  render() {
    const data = this.props.data
    return (
      <View style={styles.itemContainer}>
        <Image source={{ uri: data.gambaruri }} style={styles.itemImage} resizeMode='contain' />
        <Text numberOfLines={2} ellipsizeMode='tail' style={styles.itemName} >{data.nama}</Text>
        <Text style={styles.itemPrice}>{this.currencyFormat(Number(data.harga))}</Text>
        <Text style={styles.itemStock}>Sisa stok: {data.stock}</Text>
        <Button title='BELI' color='blue' onPress={() => this.props.actionPress(data.harga)} />
      </View>
    )
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontSize: 18,
    fontWeight: 'bold'
  },

  //? Lanjutkan styling di sini
  itemContainer: {
    width: DEVICE.width * 0.45,
    height: DEVICE.width * 0.8,
    marginTop: DEVICE.height * 0.01,
    marginBottom: DEVICE.height * 0.01,
    marginLeft: DEVICE.width* 0.01,
    marginRight: DEVICE.width * 0.01,
    justifyContent: "center",
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemImage: {
    width: DEVICE.width* 20/100,
    height: DEVICE.height * 20 / 100,
    justifyContent: "center",
    alignItems: 'center',
  },
  itemName: {
    textAlign: 'center',
    justifyContent: "center",
    alignItems: 'center',
    fontWeight: 700,
    fontSize: DEVICE.height * 0.02,
    marginBottom: DEVICE.height * 0.025
  },
  itemPrice: {
    color: 'blue',
    fontSize: DEVICE.height * 0.02,
    marginBottom: DEVICE.height * 0.007 
  },
  itemStock: {
    fontSize: DEVICE.height * 0.018, 
    marginBottom: DEVICE.height * 0.007
  },
  itemButton: {
  },
  buttonText: {
  }
})
