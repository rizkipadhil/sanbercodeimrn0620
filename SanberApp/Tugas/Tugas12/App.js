import { StatusBar } from 'expo-status-bar';
import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import Logo from './imagas/logo.png';
import VideoItem from './components/videoitem';
import data from './data.json';

let { height, width } = Dimensions.get('window');


export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image source={Logo} style={styles.header__logo} />
          <View style={styles.header__list}>
              <TouchableOpacity>
                  <Icon style={styles.header__icon} name="search" size={25} />
              </TouchableOpacity>
              <TouchableOpacity>
                  <Icon style={styles.header__icon} name="account-circle" size={25} />
              </TouchableOpacity>
          </View>
        </View>
        <View style={styles.content}>
          <ScrollView style={styles.bodyScroll}>
            <FlatList
              data={data.items}
              renderItem={(video) => <VideoItem video={video.item} />}
              keyExtractor={(item) => item.id}
              ItemSeparatorComponent={() => <View style={{ height: 0.5, backgroundColor: '#E5E5E5' }} />}
            />
          </ScrollView>
        </View>
        <View style={styles.nav}>
          <TouchableOpacity style={styles.tabItem}>
              <Icon name="home" size={25} />
              <Text style={styles.tabTitle}>
                  Home
              </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
              <Icon name="whatshot" size={25} />
              <Text style={styles.tabTitle}>
                  Trending
              </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
              <Icon name="subscriptions" size={25} />
              <Text style={styles.tabTitle}>
                    Subscription
              </Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.tabItem}>
              <Icon name="folder" size={25} />
              <Text style={styles.tabTitle}>
                    Folders
              </Text>
          </TouchableOpacity>
        </View>
        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: 55,
    backgroundColor: "white",
    flexDirection: "row",
    paddingHorizontal: width * 0.04,
    borderBottomWidth: 0.5,
    borderColor: '#e5e5e5',
    elevation: 3,
    alignItems: "center",
    justifyContent: "space-between",
  },
  header__logo: {
    // width: width*0.1,
    // height: height*0.1,
    width: 98,
    height: 22,
  },
  header__list: {
    flexDirection: "row",
  },
  header__icon:{
    marginLeft: 25,
  },    
  content: {
    flex: 1,
  },
  bodyScroll: {
    height: height*0.5,
  },
  nav: {
    height: 60,
    backgroundColor: "white",
    borderTopWidth: 0.5,
    borderColor: '#e5e5e5',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  tabTitle: {
    color: '#3c3c3c',
    fontSize: 11,
    paddingTop: 4,
  },
  title__app: {
    fontSize: 18,
    color: "#ebab34",
  },
});
