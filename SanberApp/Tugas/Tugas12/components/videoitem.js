import React, {Component} from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity } from 'react-native';
import moment from 'moment';

let { height, width } = Dimensions.get('window');

export default class VideoItem extends Component {
    nFormatter(num, digits) {
        var si = [
            { value: 1, symbol: "" },
            { value: 1E3, symbol: "k" },
            { value: 1E6, symbol: "M" },
            { value: 1E9, symbol: "G" },
            { value: 1E12, symbol: "T" },
            { value: 1E15, symbol: "P" },
            { value: 1E18, symbol: "E" }
        ];
        var rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
        var i;
        for (i = si.length - 1; i > 0; i--) {
            if (num >= si[i].value) {
                break;
            }
        }
        return (num / si[i].value).toFixed(digits).replace(rx, "$1") + si[i].symbol + ' views';
    }
    datechanger(date){
        return moment.utc(date).fromNow();
    }

    render(){
        let video = this.props.video;
        return (
          <View style={styles.container}>
            <Image
              source={{ uri: video.snippet.thumbnails.medium.url }}
              style={styles.image}
            />
            <View style={styles.desContaienr}>
              <Image
                source={{
                  uri: `https://i.pravatar.cc/${Math.floor(Math.random()*(999-100+1)+100)}`,
                }}
                style={styles.profil}
              />
              <View style={styles.videoDetails}>
                <Text numberOfLines={2} style={styles.videoTitle}>
                  {video.snippet.title}
                </Text>
                <Text style={styles.videoStats}>
                  {`${video.snippet.channelTitle} · ${this.nFormatter(
                    video.statistics.viewCount,
                    1
                  )} ${this.datechanger(video.snippet.publishedAt)}`}
                </Text>
              </View>
              <TouchableOpacity>
                <Icon
                  name="more-vert"
                  size={20}
                  color="#999999"
                  style={styles.iconmore}
                />
              </TouchableOpacity>
            </View>
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
    },
    image: {
        height: height*0.34
    },
    desContaienr: {
        flexDirection: "row",
        paddingTop: 15,
    },
    profil: {
        width: 50,
        height: 50,
        borderRadius: 25,
    },
    videoDetails: {
        paddingHorizontal: 15,
        flex: 1,
    },
    videoTitle: {
        fontSize: 16,
        color: '#3c3c3c'
    },
    videoStats: {
        color: 'grey',
        fontSize: 15,
        paddingTop: 3,
    },
    iconmore: {

    },
});
