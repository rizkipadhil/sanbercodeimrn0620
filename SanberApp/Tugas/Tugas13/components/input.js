import React from 'react';
import { View, Text, StyleSheet, TextInput, Dimensions } from 'react-native';

let { height, width } = Dimensions.get('window');

// ... rest = mengumpulkan semua props dengan rest
const Input = ({
    placeholder,
    password,
    editable = true,
    ...rest
}) => {
    return (
        <TextInput
            style={styles.wrapper__box}
            placeholder={placeholder}
            placeholderTextColor={'#D1D3D5'}
            secureTextEntry={password}
            editable={editable}
            {...rest}
        />
    );
};

const styles = StyleSheet.create({
    wrapper__box: {
        width: width * 0.8,
        height: height * 0.01,
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#D1D3D5',
        backgroundColor: '#FDFDFD',
        paddingHorizontal: 15,
        paddingVertical: 19,
        fontSize: 14,
        color: '#3B3F46',
    },
});

export default Input;
