import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { StyleSheet, Text, View, Image, Dimensions, TouchableOpacity, FlatList, ScrollView } from 'react-native';

let { height, width } = Dimensions.get('window');


const Skill = ({key, keyval, data}) => {
    const { id,
        skillName,
        category,
        categoryName,
        logoUrl,
        iconType,
        iconName,
        percentageProgress,
        decimal } = data;
    const listskill = (
        <View>
            <View style={{ flexDirection: 'row' }}>
                <Text style={styles.label__form}>{skillName}</Text>
                <Text style={styles.label__form}>({categoryName})</Text>
                <Icon name={iconName} style={{ marginTop: height * 0.01, marginLeft: width * 0.01 }} size={25} />
            </View>
            <View>
                <View style={styles.frame}>
                    <View style={{
                        backgroundColor: '#2C2E3F',
                        borderRadius: 10,
                        height: height * 0.03,
                        width: width * ((decimal/100)-0.2),
                    }} />
                </View>
            </View>
        </View>
    );

    return (
        listskill
    );
}

const styles = StyleSheet.create({
    frame: {
        backgroundColor: '#C9C9C9',
        borderRadius: 10,
        height: height * 0.03,
    },
    container: {
        flex: 1,
    },
    headerhome: {
        flex: 0.2,
        flexDirection: 'row',
        paddingTop: height * 0.045,
        paddingBottom: height * 0.015,
        paddingLeft: width * 0.055,
        paddingRight: width * 0.055,
        justifyContent: 'space-between',
    },
    header: {
        flex: 0.35,
        justifyContent: 'center',
        alignItems: 'center',
    },
    header__image__home: {
        height: height * 0.05,
        width: width * 0.34,
    },
    header__image__tagname: {
        height: height * 0.0205,
        width: width * 0.25,
        marginTop: height * 0.015,
        marginRight: width * 0.02,
    },
    header__image: {
        height: height * 0.1,
        width: width * 0.5,
    },
    contenthome: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    head_title: {
        paddingBottom: height * 0.02,
        fontSize: 25,
        fontWeight: '700',
    },
    bodyhome: {
        paddingTop: 10,
        paddingLeft: 40,
        paddingRight: 40,
        paddingBottom: 10,
    },
    label__form: {
        paddingTop: height * 0.015,
        paddingBottom: height * 0.015,
        fontSize: 16,
        color: '#000000',
        fontWeight: '700',
    },
    label__form: {
        paddingTop: height * 0.015,
        paddingBottom: height * 0.015,
        fontSize: 10,
        color: '#000000',
        fontWeight: '700',
    },
    scroll: {
        height: height * 0.5,
    },
});

export default Skill;