import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from "react-native";

import { AuthContext } from "../context";

import Icon from "react-native-vector-icons/MaterialIcons";

import Logo from "../../Tugas13/assets/logo.png";
import Homelogo from "../../Tugas13/assets/home.png";
import Foto from "../../Tugas13/assets/Foto.png";
import FB from "../../Tugas13/assets/fb.png";
import Tele from "../../Tugas13/assets/tele.png";
import Gitlab from "../../Tugas13/assets/gitlab.png";
import Back from "../../Tugas13/assets/back.png";
import Input from "../../Tugas13/components/input";
import Tugas14 from "../../Tugas14/App";
import Login from "./components/LoginScreen";
import About from "./components/AboutScreen";
import Add from "./components/AddScreen";
import Project from "./components/ProjectScreen";


let { height, width } = Dimensions.get("window");

const ScreenContainer = ({ children }) => (
  <View style={styles.container}>{children}</View>
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  button: {
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginVertical: 10,
    borderRadius: 5,
  },
});

export const SignIn = ({ navigation }) => {
    return (
      <ScreenContainer>
        <Login navigator={() => navigation.toggleDrawer()} />
      </ScreenContainer>
    );
};
export const Home = ({ navigation }) => {
  return (
    <ScreenContainer>
      <Project />
    </ScreenContainer>
  );
};
export const Details = ({ navigation }) => {
  return (
    <ScreenContainer>
      <Tugas14 />
    </ScreenContainer>
  );
};
export const CreateAccount = ({ navigation }) => {
  return (
    <ScreenContainer>
      <Add />
    </ScreenContainer>
  );
};
export const Profile = ({ navigation }) => {
  return (
    <ScreenContainer>
      <About navigator={navigation} />
    </ScreenContainer>
  );
};

export const Splash = () => (
  <ScreenContainer>
    <Text>Loading...</Text>
  </ScreenContainer>
);