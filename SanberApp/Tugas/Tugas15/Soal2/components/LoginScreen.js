
import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
  ScrollView,
} from "react-native";

import { AuthContext } from "../../context";

import Icon from "react-native-vector-icons/MaterialIcons";

import Logo from "../../../Tugas13/assets/logo.png";
import Homelogo from "../../../Tugas13/assets/home.png";
import Foto from "../../../Tugas13/assets/Foto.png";
import FB from "../../../Tugas13/assets/fb.png";
import Tele from "../../../Tugas13/assets/tele.png";
import Gitlab from "../../../Tugas13/assets/gitlab.png";
import Back from "../../../Tugas13/assets/back.png";
import Input from "../../../Tugas13/components/input";

let { height, width } = Dimensions.get("window");

const LoginScreen = ({ navigator }) => {
  const { signIn } = React.useContext(AuthContext);
  const [page, setPage] = useState("home");
  const [formLogin, setFormLogin] = useState({
    email: "rizkipadhil@gmail.com",
    password: "123456",
  });
  const [formRegister, setFormRegister] = useState({
    email: "rizkipadhil@gmail.com",
    name: "Rizki Padhil",
    username: "rizkipadhil",
    password: "123456",
    password_confirm: "123456",
  });

  const onInputChange = (value, input) => {
    setFormLogin({
      ...formLogin,
      [input]: value,
    });
  };
  const onInputChangeRegister = (value, input) => {
    setFormRegister({
      ...formRegister,
      [input]: value,
    });
  };

  return (
    <View style={styleslogin.container}>
      <>
        <View style={styleslogin.header}>
          <Image source={Logo} style={styleslogin.header__image} />
        </View>
        <View style={styleslogin.content}>
          <View stlye={styleslogin.content__head}>
            <Text style={styleslogin.head_title}>Login Form</Text>
          </View>
          <View style={styleslogin.body}>
            <View>
              <Text style={styleslogin.label__form}>E-mail</Text>
              <View>
                <Input
                  placeholder="E-mail Address"
                  password={false}
                  value={formLogin.email}
                  onChangeText={(value) => onInputChange(value, "email")}
                />
              </View>
            </View>
            <View>
              <Text style={styleslogin.label__form}>Password</Text>
              <View>
                <Input
                  placeholder="Password"
                  password={true}
                  value={formLogin.password}
                  onChangeText={(value) => onInputChange(value, "password")}
                />
              </View>
            </View>
          </View>
          <View style={styleslogin.footer}>
            {/* <TouchableOpacity onPress={() => navigator}>
              <Text style={styleslogin.btn_black}>Menu Lain</Text>
            </TouchableOpacity> */}
            <TouchableOpacity onPress={() => signIn()}>
              <Text style={styleslogin.btn_blue}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
      </>
    </View>
  );
};

const styleslogin = StyleSheet.create({
  container: {
    flex: 1,
  },
  headerhome: {
    flex: 0.2,
    paddingTop: height * 0.015,
    paddingBottom: height * 0.015,
    paddingLeft: width * 0.015,
  },
  header: {
    flex: 0.35,
    justifyContent: "center",
    alignItems: "center",
  },
  header__image__home: {
    height: height * 0.05,
    width: width * 0.34,
  },
  header__image__pp: {
    height: height * 0.195,
    width: width * 0.437,
  },
  header__image: {
    height: height * 0.1,
    width: width * 0.5,
  },
  content: {
    flex: 1,
    padding: 40,
  },
  contenthome: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  content__head__home: {},
  head_title: {
    paddingBottom: height * 0.02,
    fontSize: 25,
    fontWeight: 500,
  },
  body: {},
  bodyhome: {
    paddingTop: 10,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10,
  },
  label__form: {
    paddingTop: height * 0.015,
    paddingBottom: height * 0.015,
    fontSize: 16,
    color: "#9D9D9D",
  },
  footerhome: {
    paddingBottom: 40,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 20,
  },
  footer: {
    flex: 1,
    paddingBottom: 40,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 20,
  },
  btn_black: {
    padding: width * 0.09,
    paddingTop: height * 0.01,
    paddingBottom: height * 0.01,
    backgroundColor: "#333333",
    borderRadius: 6,
    fontSize: 15,
    color: "#FFFFFF",
  },
  btn_blue: {
    padding: width * 0.09,
    paddingTop: height * 0.01,
    paddingBottom: height * 0.01,
    backgroundColor: "#18A0FB",
    borderRadius: 6,
    fontSize: 15,
    color: "#FFFFFF",
  },
  btn_icon: {
    flex: 1,
    flexDirection: "row",
  },
  scroll: {
    height: height * 0.5,
  },
});

export default LoginScreen;