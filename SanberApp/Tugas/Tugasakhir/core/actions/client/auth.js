const setDataUser = (payload) => {
  return {
    type: 'SET_DATA_USER',
    payload: payload,
  };
};
export {setDataUser};
