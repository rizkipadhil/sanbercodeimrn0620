const setDetail = (payload) => {
  return {
    type: 'SET_DETAIL',
    payload: payload,
  };
};
export { setDetail };
