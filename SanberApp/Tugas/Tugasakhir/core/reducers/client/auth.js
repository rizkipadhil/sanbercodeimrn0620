export default function (state = [], action) {
  switch (action.type) {
    case 'SET_DATA_USER':
      state = action.payload;
      return state;
    default:
      return state;
  }
}