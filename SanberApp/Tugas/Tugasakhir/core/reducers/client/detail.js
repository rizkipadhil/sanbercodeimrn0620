export default function (state = [], action) {
  switch (action.type) {
    case 'SET_DETAIL':
      state = action.payload;
      return state;
    default:
      return state;
  }
}
