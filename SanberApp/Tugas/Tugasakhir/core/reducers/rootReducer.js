import {combineReducers} from 'redux';
import auth from './client/auth';
import detail from './client/detail';
export default combineReducers({
  auth,
  detail,
});
