import * as React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import 'react-native-gesture-handler';

import LoginScreen from './pages/LoginScreen';
import Detail from './pages/DetailScreen';
import tabhome from './tabhome';

const Stack = createStackNavigator();

const DetailScreen = ({ navigation }) => {
    return <Detail navigator={navigation} />;
};

export default class App extends React.Component {
    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Login" screenOptions={{
                    headerShown: false
                }}>
                    <Stack.Screen name='Login' component={LoginScreen} />
                    <Stack.Screen name='Home' component={tabhome} />
                    <Stack.Screen name='Detail' component={DetailScreen} />
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}
