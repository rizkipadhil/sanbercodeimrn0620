
import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
    ScrollView,
} from "react-native";

import { AuthContext } from "../context";

import Icon from "react-native-vector-icons/MaterialIcons";

import Logo from "../../Tugas13/assets/logo.png";
import Homelogo from "../../Tugas13/assets/home.png";
import Foto from "../../Tugas13/assets/Foto.png";
import FB from "../../Tugas13/assets/fb.png";
import Tele from "../../Tugas13/assets/tele.png";
import Gitlab from "../../Tugas13/assets/gitlab.png";
import Back from "../../Tugas13/assets/back.png";
import Input from "../../Tugas13/components/input";

let { height, width } = Dimensions.get("window");

const AboutScreen = ({ navigator }) => {
    const [page, setPage] = useState("home");
    const [formLogin, setFormLogin] = useState({
        email: "rizkipadhil@gmail.com",
        password: "123456",
    });
    const [formRegister, setFormRegister] = useState({
        email: "rizkipadhil@gmail.com",
        name: "Rizki Padhil",
        username: "rizkipadhil",
        password: "123456",
        password_confirm: "123456",
    });

    const onInputChange = (value, input) => {
        setFormLogin({
            ...formLogin,
            [input]: value,
        });
    };
    const logout = () => {
        navigator.navigate('Login');
    };
    const Home = (
        <>
            <View style={stylesAbout.headerhome}>
                <Image source={Homelogo} style={stylesAbout.header__image__home} />
            </View>
            <View style={stylesAbout.contenthome}>
                <View stlye={stylesAbout.content__head__home}>
                    <Image source={Foto} style={stylesAbout.header__image__pp} />
                </View>
            </View>
            <ScrollView style={stylesAbout.scroll}>
                <View style={stylesAbout.bodyhome}>
                    <View>
                        <Text style={stylesAbout.label__form}>E-mail</Text>
                        <View>
                            <Input
                                placeholder="E-mail Address"
                                password={false}
                                value={formRegister.email}
                                editable={false}
                                onChangeText={(value) =>
                                    onInputChange(value, 'email')
                                }
                            />
                        </View>
                    </View>
                    <View>
                        <Text style={stylesAbout.label__form}>Name</Text>
                        <View>
                            <Input
                                placeholder="Name"
                                password={false}
                                value={formRegister.name}
                                editable={false}
                                onChangeText={(value) =>
                                    onInputChange(value, 'name')
                                }
                            />
                        </View>
                    </View>
                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={stylesAbout.label__form}>Gitlab</Text>
                            <Image source={Gitlab} style={{ height: 15, width: 15, marginTop: height * 0.017, marginLeft: width * 0.01 }} />
                        </View>
                        <View>
                            <Input
                                placeholder=""
                                password={false}
                                value={formRegister.username}
                                editable={false}
                                onChangeText={(value) =>
                                    onInputChange(value, 'username')
                                }
                            />
                        </View>
                    </View>
                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={stylesAbout.label__form}>Telegram</Text>
                            <Image source={Tele} style={{ height: 15, width: 15, marginTop: height * 0.017, marginLeft: width * 0.01 }} />
                        </View>
                        <View>
                            <Input
                                placeholder=""
                                password={false}
                                value={formRegister.username}
                                editable={false}
                                onChangeText={(value) =>
                                    onInputChange(value, 'username')
                                }
                            />
                        </View>
                    </View>
                    <View>
                        <View style={{ flexDirection: 'row' }}>
                            <Text style={stylesAbout.label__form}>Facebook</Text>
                            <Image source={FB} style={{ height: 15, width: 15, marginTop: height * 0.017, marginLeft: width * 0.01 }} />
                        </View>
                        <View>
                            <Input
                                placeholder=""
                                password={false}
                                value={formRegister.name}
                                editable={false}
                                onChangeText={(value) =>
                                    onInputChange(value, 'name')
                                }
                            />
                        </View>
                    </View>
                    <View style={stylesAbout.footerhome}>
                        <TouchableOpacity onPress={() => logout()}>
                            <Text style={stylesAbout.btn_black}>
                                <Image source={Back} style={{ height: 12, width: 12, marginTop: height * 0.001, marginRight: width * 0.01 }} />
                                Logout
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </>
    );

    return Home;
};

const stylesAbout = StyleSheet.create({
    container: {
        flex: 1,
    },
    headerhome: {
        flex: 0.2,
        paddingTop: height * 0.015,
        paddingBottom: height * 0.015,
        paddingLeft: width * 0.015,
    },
    header: {
        flex: 0.35,
        justifyContent: "center",
        alignItems: "center",
    },
    header__image__home: {
        height: height * 0.05,
        width: width * 0.34,
    },
    header__image__pp: {
        marginTop: height*0.08,
        height: height * 0.194,
        width: width * 0.36,
    },
    header__image: {
        height: height * 0.1,
        width: width * 0.5,
    },
    content: {
        flex: 1,
        padding: 40,
    },
    contenthome: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: height * 0.08,
    },
    content__head__home: {},
    head_title: {
        paddingBottom: height * 0.02,
        fontSize: 25,
        fontWeight: 500,
    },
    body: {},
    bodyhome: {
        paddingTop: 10,
        paddingLeft: 40,
        paddingRight: 40,
        paddingBottom: 10,
    },
    label__form: {
        paddingTop: height * 0.015,
        paddingBottom: height * 0.015,
        fontSize: 16,
        color: "#9D9D9D",
    },
    footerhome: {
        paddingBottom: 40,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 20,
    },
    footer: {
        flex: 1,
        paddingBottom: 40,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 20,
    },
    btn_black: {
        padding: width * 0.09,
        paddingTop: height * 0.01,
        paddingBottom: height * 0.01,
        backgroundColor: "#333333",
        borderRadius: 6,
        fontSize: 15,
        color: "#FFFFFF",
    },
    btn_blue: {
        padding: width * 0.09,
        paddingTop: height * 0.01,
        paddingBottom: height * 0.01,
        backgroundColor: "#18A0FB",
        borderRadius: 6,
        fontSize: 15,
        color: "#FFFFFF",
    },
    btn_icon: {
        flex: 1,
        flexDirection: "row",
    },
    scroll: {
        height: height * 0.5,
    },
});

export default AboutScreen;