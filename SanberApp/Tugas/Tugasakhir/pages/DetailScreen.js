
import React, { useState, useEffect } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
    ScrollView,
} from "react-native";
import axios from 'axios';
import { AuthContext } from "../context";

import Icon from "react-native-vector-icons/MaterialIcons";
import Back from "../../Tugas13/assets/back.png";
import Homelogo from "../../Tugas13/assets/home.png";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";

let { height, width } = Dimensions.get("window");

const AboutScreen = ({ navigator }) => {
    const [artikel, setArtikel] = useState({});
    const [loading, setLoading] = useState(false);
    const detail = useSelector((state) => state.data.detail);

    useEffect(() => {
        getData();
    }, [loading]);

    const getData = () => {
        console.log(detail);
        setLoading(true);
    }

    const dateIndo = (datecall) => {
        var date = moment.utc(datecall);
        return date.format("DD-MM-YYYY");
    }

    const Back = () => {
        navigator.goBack();
    };

    const Home = (
      <View style={stylesAbout.container}>
        <>
          <View style={stylesAbout.headerhome}>
            <Image source={Homelogo} style={stylesAbout.header__image__home} />
          </View>
          <ScrollView style={stylesAbout.scroll}>
            <View style={stylesAbout.bodyhome}>
              <View style={stylesAbout.item}>
                <Image
                  source={{ uri: detail.urlToImage }}
                  style={stylesAbout.image_item}
                />
                <View style={stylesAbout.contentitem}>
                  <Text style={stylesAbout.left}>{detail.souce}</Text>
                  <Text style={stylesAbout.right}>{detail.date}</Text>
                </View>
              </View>
              <View style={{ paddingLeft: width*0.02, paddingRight: width*0.02 }}>
                <View
                  style={{
                    paddingTop: height * 0.02,
                    paddingBottom: height * 0.01,
                  }}
                >
                  <Text style={{ fontSize: 24, fontWeight: 'bold' }}>{detail.title}</Text>
                </View>
                <View>
                  <Text>{detail.content}</Text>
                </View>
              </View>
            </View>
            <View style={stylesAbout.footerhome}>
              <TouchableOpacity onPress={() => Back()}>
                <Text style={stylesAbout.btn_black}>Back</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </>
      </View>
    );

    return Home;
};

const stylesAbout = StyleSheet.create({
  container: {
    flex: 1,
  },

  contenthome: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: height * 0.08,
  },
  content__head__home: {},
  head_title: {
    paddingBottom: height * 0.02,
    fontSize: 25,
    fontWeight: 500,
  },
  header__image__pp: {
    marginTop: height * 0.08,
    height: height * 0.194,
    width: width * 0.36,
  },
  header__image__home: {
    height: height * 0.05,
    width: width * 0.34,
  },
  HeaderHome: {
    paddingTop: 30,
    paddingLeft: 40,
    paddingRight: 40,
    paddingBottom: 10,
  },
  TextHeader: {
    fontSize: height * 0.2,
    fontWeight: 900,
  },
  body: {},
  bodyhome: {
    marginTop: height * 0.03,
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  scroll: {
    height: height * 0.5,
  },
  image_item: {
    width: width * 0.9,
    height: height * 0.3,
  },
  item: {
    marginBottom: height * 0.02,
  },
  contentitem: {
    flex: 1,
    backgroundColor: "#EBE8E8",
    flexDirection: "row",
    paddingTop: height * 0.01,
    paddingLeft: width * 0.02,
    paddingLeft: width * 0.02,
    paddingRight: width * 0.02,
    paddingBottom: height * 0.01,
    marginTop: height * -0.045,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
  },
  left: {
    fontSize: height * 0.023,
    fontWeight: "bold",
  },
  right: {
    flex: 1,
    textAlign: "right",
    fontWeight: "bold",
  },
  footerhome: {
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 40,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 20,
  },
  footer: {
    flex: 1,
    paddingBottom: 40,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingTop: 20,
  },
  btn_black: {
    padding: width * 0.09,
    paddingTop: height * 0.01,
    paddingBottom: height * 0.01,
    backgroundColor: "#333333",
    borderRadius: 6,
    fontSize: 15,
    color: "#FFFFFF",
  },
  btn_blue: {
    padding: width * 0.09,
    paddingTop: height * 0.01,
    paddingBottom: height * 0.01,
    backgroundColor: "#18A0FB",
    borderRadius: 6,
    fontSize: 15,
    color: "#FFFFFF",
  },
  btn_icon: {
    flex: 1,
    flexDirection: "row",
  },
});

export default AboutScreen;