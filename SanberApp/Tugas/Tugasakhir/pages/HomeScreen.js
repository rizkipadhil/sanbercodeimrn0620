
import React, { useState, useEffect, useRef } from "react";
import {
    StyleSheet,
    Text,
    View,
    Image,
    Dimensions,
    TouchableOpacity,
    FlatList,
    ScrollView,
} from "react-native";
import axios from 'axios';
import { AuthContext } from "../context";

import Icon from "react-native-vector-icons/MaterialIcons";
import Homelogo from "../../Tugas13/assets/home.png";
import moment from "moment";
import { useDispatch, useSelector } from "react-redux";
import { setDetail } from "../core/actions/client/detail";

let { height, width } = Dimensions.get("window");

const AboutScreen = ({ navigator }) => {
    const [artikel, setArtikel] = useState([]);
    const [update, setUpdate] = useState(false);
    const detail = useSelector((state) => state.data.detail);
    const dispatch = useDispatch();

    useEffect(()=>{
        callArtikel();
    }, [update])
    const callArtikel = async () => {
        const data = await axios.get('http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=1049d5002c3043cfbcad3bf6b2f3099e');
        console.log(data.data);
        setArtikel(data.data.articles);
        setUpdate(true);
    }

    const dateIndo = (datecall) => {
        var date = moment.utc(datecall);
        return date.format("DD-MM-YYYY");
    }

    const detailPage = (data, key) => {
        dispatch(
          setDetail({
            urlToImage: data.urlToImage,
            souce: data.source.name,
            content: data.content,
            title: data.title,
            date: dateIndo(data.publishedAt),
            index: key,
          })
        );
        setTimeout(() => {
            navigator.navigate('Detail');
        }, 100);
    };

    const Home = (
        <View style={stylesAbout.container}>
            <>
                <View style={stylesAbout.headerhome}>
                    <Image source={Homelogo} style={stylesAbout.header__image__home} />
                </View>
                <ScrollView style={stylesAbout.scroll}>
                    <View style={stylesAbout.bodyhome}>
                        {artikel.length > 0 && (
                            <>
                                {artikel.map((art, index) => {
                                    return (
                                        <TouchableOpacity key={index} onPress={() => detailPage(art, index)} style={stylesAbout.item}>
                                            <Image source={{ uri: art.urlToImage }} style={stylesAbout.image_item} />
                                            <View style={stylesAbout.contentitem}>
                                                <Text style={stylesAbout.left}>
                                                   {art.source.name}
                                                </Text>
                                                <Text style={stylesAbout.right}>
                                                    {dateIndo(art.publishedAt)}
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    );
                                })}
                            </>
                        )}
                    </View>
                </ScrollView>
            </>
        </View>
    );

    return Home;
};

const stylesAbout = StyleSheet.create({
    container: {
        flex: 1,
    },

    contenthome: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: height * 0.08,
    },
    content__head__home: {},
    head_title: {
        paddingBottom: height * 0.02,
        fontSize: 25,
        fontWeight: 500,
    },
    header__image__pp: {
        marginTop: height * 0.08,
        height: height * 0.194,
        width: width * 0.36,
    },
    header__image__home: {
        height: height * 0.05,
        width: width * 0.34,
    },
    HeaderHome:{
        paddingTop: 30,
        paddingLeft: 40,
        paddingRight: 40,
        paddingBottom: 10,
    },
    TextHeader:{
        fontSize: height*0.2,
        fontWeight: 900,
    },
    body: {},
    bodyhome: {
        marginTop: height*0.03,
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
    },
    scroll: {
        height: height * 0.5,
    },
    image_item: {
        width: width*0.9,
        height: height*0.3,
    },
    item: {
        marginBottom: height*0.02,
    },
    contentitem:{
        flex: 1,
        backgroundColor: '#EBE8E8',
        flexDirection: 'row',
        paddingTop: height*0.01,
        paddingLeft: width*0.02,
        paddingLeft: width*0.02,
        paddingRight: width*0.02,
        paddingBottom: height * 0.01,
        marginTop: height * -0.045,
        borderTopLeftRadius: 10,
        borderTopRightRadius: 10,
    },
    left:{
        fontSize: height*0.023,
        fontWeight: 'bold',
    },
    right:{
        flex: 1,
        textAlign: 'right',
        fontWeight: 'bold',
    },
});

export default AboutScreen;