import React from 'react';
import { View, Text, TextInput, StyleSheet, Button, Dimensions, TouchableOpacity, Image } from 'react-native';
import MaterialCommunityIcons from "@expo/vector-icons/MaterialCommunityIcons";
// import MaterialCommunityIcons from "react-native-vector-icons/MaterialCommunityIcons";

import Icon from "react-native-vector-icons/MaterialIcons";

import Logo from "../../Tugas13/assets/logo.png";
import Homelogo from "../../Tugas13/assets/home.png";
import Foto from "../../Tugas13/assets/Foto.png";
import FB from "../../Tugas13/assets/fb.png";
import Tele from "../../Tugas13/assets/tele.png";
import Gitlab from "../../Tugas13/assets/gitlab.png";
import Back from "../../Tugas13/assets/back.png";
import Input from "../../Tugas13/components/input";

let { height, width } = Dimensions.get("window");


export default class LoginScreen extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            userName: '',
            password: '',
            isError: false,
            isErrorUsername: false,
        }
    }

    signIn() {
        console.log(this.state.userName, ' ', this.state.password)
        //? #Soal No. 1 (10 poin)
        //? Buatlah sebuah fungsi untuk berpindah halaman hanya jika password yang di input bernilai '12345678' 
        //? dan selain itu, maka akan mengubah state isError menjadi true dan tidak dapat berpindah halaman.
        const checkpassword = this.state.password === '12345678' ? false : true;
        this.setState({ isError: checkpassword })
        //? #SoalTambahan (+ 5 poin): kirimkan params dengan key => userName dan value => this.state.userName ke halaman Home, 
        //? dan tampilkan userName tersebut di halaman Home setelah teks "Hai,"
        const checkusername = this.state.userName !== '' ? false : true;
        this.setState({ isErrorUsername: checkusername })
        // Kode di sini
        if (checkpassword === false && checkusername === false) {
            this.setState({
                userName: '',
                password: '',
            })
            this.props.navigation.push('Home', {
                userName: this.state.userName,
            });
        }
    }

    render() {
        return (
            <View style={styleslogin.container}>
                <>
                    <View style={styleslogin.header}>
                        <Image source={Logo} style={styleslogin.header__image} />
                    </View>
                    <View style={styleslogin.content}>
                        <View stlye={styleslogin.content__head}>
                            <Text style={styleslogin.head_title}>Login Form</Text>
                        </View>
                        <View style={styleslogin.body}>
                            <View>
                                <Text style={styleslogin.label__form}>E-mail</Text>
                                <View>
                                    <TextInput
                                        style={styleslogin.wrapper__box_input}
                                        placeholder='Masukkan Nama User/Email'
                                        onChangeText={userName => this.setState({ userName })}
                                    />
                                </View>
                            </View>
                            <View>
                                <Text style={styleslogin.label__form}>Password</Text>
                                <View>
                                    <TextInput
                                        style={styleslogin.wrapper__box_input}
                                        placeholder='Masukkan Password'
                                        onChangeText={password => this.setState({ password })}
                                        secureTextEntry={true}
                                    />
                                </View>
                            </View>
                        </View>
                        <View style={styleslogin.footer}>
                            <TouchableOpacity onPress={() => this.signIn()}>
                                <Text style={styleslogin.btn_blue}>Login</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </>
            </View>
        )
    }
};

const styleslogin = StyleSheet.create({
    wrapper__box_input: {
        width: width * 0.8,
        height: height * 0.01,
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 5,
        borderColor: '#D1D3D5',
        backgroundColor: '#FDFDFD',
        paddingHorizontal: 15,
        paddingVertical: 19,
        fontSize: 14,
        color: '#3B3F46',
    },
    container: {
        flex: 1,
    },
    headerhome: {
        flex: 0.2,
        paddingTop: height * 0.015,
        paddingBottom: height * 0.015,
        paddingLeft: width * 0.015,
    },
    header: {
        flex: 0.35,
        justifyContent: "center",
        alignItems: "center",
    },
    header__image__home: {
        height: height * 0.05,
        width: width * 0.34,
    },
    header__image__pp: {
        height: height * 0.195,
        width: width * 0.437,
    },
    header__image: {
        height: height * 0.1,
        width: width * 0.5,
    },
    content: {
        flex: 1,
        padding: 40,
    },
    contenthome: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    content__head__home: {},
    head_title: {
        paddingBottom: height * 0.02,
        fontSize: 25,
    },
    body: {},
    bodyhome: {
        paddingTop: 10,
        paddingLeft: 40,
        paddingRight: 40,
        paddingBottom: 10,
    },
    label__form: {
        paddingTop: height * 0.015,
        paddingBottom: height * 0.015,
        fontSize: 16,
        color: "#9D9D9D",
    },
    footerhome: {
        paddingBottom: 40,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 20,
    },
    footer: {
        flex: 1,
        paddingBottom: 40,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingTop: 20,
    },
    btn_black: {
        padding: width * 0.09,
        paddingTop: height * 0.01,
        paddingBottom: height * 0.01,
        backgroundColor: "#333333",
        borderRadius: 6,
        fontSize: 15,
        color: "#FFFFFF",
    },
    btn_blue: {
        padding: width * 0.09,
        paddingTop: height * 0.01,
        paddingBottom: height * 0.01,
        backgroundColor: "#18A0FB",
        borderRadius: 6,
        fontSize: 15,
        color: "#FFFFFF",
    },
    btn_icon: {
        flex: 1,
        flexDirection: "row",
    },
    scroll: {
        height: height * 0.5,
    },
});