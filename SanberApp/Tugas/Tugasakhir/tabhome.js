import React from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from './pages/HomeScreen'
import About from './pages/AboutScreen'

const Tab = createBottomTabNavigator();

const HomePage = ({ navigation }) => {
    return <Home navigator={navigation} />;
};
const AboutPage = ({ navigation }) => {
    return <About navigator={navigation} />;
};

const HomeTabs = () => {
    return (
        <Tab.Navigator>
            <Tab.Screen
                name="Home"
                component={HomePage}
            />
            <Tab.Screen
                name="About"
                component={AboutPage}
            />
        </Tab.Navigator>
    );
};

export default HomeTabs;
