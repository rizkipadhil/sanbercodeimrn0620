import {createStore, combineReducers, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';

import data from '../Tugas/Tugasakhir/core/reducers/rootReducer';

const reducer = combineReducers({data});

const store = createStore(reducer, applyMiddleware(thunk));

export default store;
