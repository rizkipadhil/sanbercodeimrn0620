// Tugas 6 – Object Literal
// case 1
console.log('Soal 1');
function arrayToObject(arr) {
    // Code di sini 
    let makeText = '';
    if(arr.length > 0) {
        arr.forEach((element, index) => {
            let makeObject = {};
            makeObject.firstName = element[0]
            makeObject.lastName = element[1]
            makeObject.gender = element[2]
            makeObject.age = element[3] ? element[3] : 'Invalid Birth Year';
            makeText += `${index+1}. ${makeObject.firstName} ${makeObject.lastName}: ${JSON.stringify(makeObject)} \n`;
        });
    }else{
        makeText = '""';
    }
    console.log(makeText);
}

// Driver Code
var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)
/*
    1. Bruce Banner: { 
        firstName: "Bruce",
        lastName: "Banner",
        gender: "male",
        age: 45
    }
    2. Natasha Romanoff: { 
        firstName: "Natasha",
        lastName: "Romanoff",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)
/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

// Error case 
arrayToObject([]) // ""

// case 2
console.log('soal 2');
const item = [
    {
        nama: 'Sepatu Stacattu',
        harga: 1500000
    },
    {
        nama: 'Baju Zoro',
        harga: 500000
    },
    {
        nama: 'Baju H&N',
        harga: 250000
    },
    {
        nama: 'Sweater Uniklooh',
        harga: 175000
    },
    {
        nama: 'Casing Handphone',
        harga: 50000
    },
];
function searchData(uang) {
    let data = [];
    let money = uang;
    let result = {};
    item.forEach(element => {
        const kurangin = money - element.harga;
        let checkKecukupan = kurangin >= 0 ? true : false;
        if (element.harga <= money && checkKecukupan) {
            data.push(element.nama);
            money -= element.harga;
        }
    });
    result = {
        data: data,
        sisa: money,
    };
    return result;
}
function shoppingTime(memberId, money) {
    // you can only write your code here!
    const objectdata = {};
    if(!memberId) return "Mohon maaf, toko X hanya berlaku untuk member saja";
    objectdata.memberId = memberId;
    objectdata.money = money;
    const finddata = searchData(money);
    objectdata.listPurchased = finddata.data;
    if(objectdata.listPurchased.length < 1) return "Mohon maaf, uang tidak cukup";
    objectdata.changeMoney = finddata.sisa;
    return objectdata;
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

// case 3
console.log('Soal 3')
const rute = ['A', 'B', 'C', 'D', 'E', 'F'];
function checkBayar(dari, sampe){
    let naikdari = rute.indexOf(dari);
    let tujuan = rute.indexOf(sampe);
    const harga = (tujuan - naikdari)*2000;
    return harga;
}
function naikAngkot(arrPenumpang) {
    //your code here
    let data = [];
    if(arrPenumpang.length < 1) return [];
    arrPenumpang.forEach(element => {
            let makedata = {};
            makedata.penumpang = element[0];
            makedata.naikDari = element[1];
            makedata.tujuan = element[2];
            makedata.bayar = checkBayar(element[1], element[2]);
            data.push(makedata);
    });
    return data;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]